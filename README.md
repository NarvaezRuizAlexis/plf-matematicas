# Conjuntos, Aplicaciones y funciones (2002)

```plantuml
@startmindmap

*[#Orange] Conjunto 
**[#lightgreen] Teoria desarrollada
***_ por
**** Georg Cantor
***_ en el
**** Siglo XIX

**[#lightgreen] piezas basicas 
***_ un
**** cojunto y elemento.
**** Relacion
**** Se da por intuitivo
**** conceptos.


***_ Las 
**** Operaciones 
*****_ la 
****** implementacion 
****** interceccion 
****** complementacion 

**[#lightgreen] Propiedades
***_ la
**** Asociativa
**** Conmutativa
**** Idempotente
**** Absorción
**** Distributiva
**** Neutralidad
**** Complementación
**** Ley de De Morgan


**[#lightgreen] Universal
***_ es
**** formado
***** todos los objetos de \nestudio en un contexto dado
***_ las
**** Propiedades
*****_ son
****** Todo conjunto A \nes subconjunto de U
****** Unión del conjunto A \ncon el conjunto universal U
******* igual a U

**[#lightgreen] Vacio
***_ surge de
**** Necesidad
***_ no
**** posee elemento alguno
***** es único

**[#lightgreen] Representacion 
***_ por
**** Diagrama de Venn
***** Encierran a los elmentos
***** Representan las opreaciones
***_ es
**** Util para visualizar resultados

**[#lightgreen] Cardinal 
***_ el 
**** Numero de elmentos que lo conforman
***_ la
**** formula
*****_ es la
****** Cadinal de una union.
****** Acotacion de cardinales.

**[#lightgreen] Aplicaciones.
***_ en
**** proceso de cambio
**** Concepto de transformacion
**** Disyunción
**** Biyectiva 

**[#lightgreen] Imagen 
***_ es el 
**** campo de valores o rango


**[#lightgreen] Imagen invera
***_ es el  
**** conjunto le hace \ncorresponder otro conjunto

**[#lightgreen] Funcion 
***_ es el
**** conjunto de los puntos
***_ la
**** Graficade la funcion

**[#lightgreen] Áreas de estudio
***_ en

**** Teoría de \nconjuntos combinatoria
*****_ la
****** extensiones de\ncombinatoria 
******* finita 
******** conjuntos infinitos

**** Teoría descriptiva \nde conjuntos
*****_ el
****** estudio de los subconjuntos
*******_ la
******** recta real
*******_ los
******** espacios polacos

**** Teoría de \nconjuntos difusos
*****_ el 
****** Elemento 
******* con grado \nde pertenencia


**** Teoría del \nmodelo interno
*****_ suele
****** utilizarse
*******_ en
******** demostrar resultados \nde consistencia

@endmindmap
```

# Funciones (2010)

```plantuml
@startmindmap

*[#Orange] Funciones
**_ son el 

***[#lightgreen] Corazón de las matemáticas
****_ el
***** Plano de Descartes
**** Con puntos 
*****_ se
****** Crea la gráfica \nde la función

**[#lightgreen] Concepto
*** Transformar 
****_ un 
***** Conjunto a otro
****_ la 
***** Regla de cambios 
******_ con
******* números reales

**[#lightgreen] Surge del cambio
***_ un
**** ejemplo
*****_ la 
****** Temperatura 
*****_ el
****** Cambio de divisas

**[#lightgreen] Un conocimiento 
***_ en 
**** Base al cálculo
***_ el 
**** Cálculo diferencial 
*****_ la
****** Derivada
*******_ es la
******** Aproximación a \nfunción más simple
***** resolver un problema
****** Función lineal 

**[#lightgreen] Características

*** Limite 
****_ el
***** Acercamiento \na un valor 
****_ No 
***** rebasa el valor

*** Función Intuitiva 
****_ la
***** Existencia de valores

****** Mínimos 
******* Función
******** De máximo a mínimo
******** Decreciendo

****** Máximos 
******* Función 
******** Pasa por un máximo
******** Creciendo

*** Funciones Decrecientes 
****_ el
***** Opuestos de un valor
****** Disminuye X y \nsus imágenes

*** Funciones Crecientes 
**** Aumento  
*****_ de  
****** Variables del primer conjunto  
****** X y sus imágenes

**[#lightgreen] inyectiva 
*** si las imágenes de elementos\n distintos son distintas

**[#lightgreen] sobreyectiva
*** si su imagen es \nigual a su codominio

**[#lightgreen] Inversa
*** otra función que al \ncomponerla con ella resulte \nen la identidad

**_ las 
*** Variables 

**** Dependiente
*****_ es
****** dependiente a otra maginitud
*******_ ejemplo 
******** Temperatura
*****_ tambien
****** Llamada como
******* experimental
******* variable de medición
******* variable de respuesta


**** Independientes
*****_ es
****** define la \nvaribale dependiente
*******_ ejemplo 
******** La hora    
******** Onda de la luz    

*****_ tambien 
****** Llamada
******* variable controlada 
******* variable predictiva 
********_ esto 
********* depende del estudio.

** Extra
***_ las 
**** Funciones algebraicas
*****_ divididas en 
****** Funciones explícitas
****** Funciones implícitas
****** Funciones polinómicas
****** Funciones racionales
****** Funciones irracionales o radicales
****** Funciones definidas a trozos

**** Funciones trascendentes
*****_ divididas en
****** Funciones exponenciales
****** Funciones logarítmicas
******  Funciones trigonométricas


@endmindmap
```
[Concepto de función matematica](https://concepto.de/funcion-matematica/)
[Variables independiente y dependientes](https://www.greelane.com/es/ciencia-tecnolog%c3%ada-matem%c3%a1ticas/ciencia/independent-and-dependent-variable-examples-606828/)


[variables-dependiente-independiente](https://www.lifeder.com/variables-dependiente-independiente/)

[Tipos de funciones matemáticas](https://psicologiaymente.com/miscelanea/tipos-de-funciones-matematicas)

# La matemática del computador (2002)

```plantuml
@startmindmap
*[#Orange] La matemática \ndel computador+

**[#lightgreen] Computador
***_ las  
**** matemáticas  
*****_ la 
****** base del funcionamiento

**[#lightgreen] Aritmética del computador
***_ los
**** Números
***** Fraccionales
******_ son la 
******* división 
******** dos números naturales


***** Decimales
****** Finitos
****** Infinitos

***** Naturales
***** truncar un número 
****** Cortar un numero (cifras)
****** Cortar a la derecha 
****** Ejemplo
******* 3.14159

**** pequeños 
*****_ en
****** potencia de 10(negativo)

**** muy grandes
*****_ en
****** potencia de 10


**** redondear un número
*****_ es 
****** Corregir error cometido 
*****_ los
****** digitos significativos 

****** truncamiento refinado
***** Ejemplo
****** 3.1416

**[#lightgreen] Sistema binario 
*** Ceros y unos
***_ la 
**** petenencia
**** no pertenecnia 

***_ en
**** computadoras
***** Construyen 
****** videos
****** imagenes


***_ un  
**** sistema mas simple 
*****_ la 
****** lógica booleana
******* Verdadero
******* Falso

*****_ el 
****** mundo de la electrónica 
******* estructuras complejas


**[#lightgreen] Aritmética precisión finita
***_  No se 
**** puede \nrepresentar infinitas cifras
***_ los
**** dígitos significativos
 
** sistema octal 
***_ en 
**** Base 8
***_ su
**** Ventaja
*****_ es 
****** no  utilizar otros símbolos\n diferentes de los dígitos
*******_ del 
******** 0 al 7 
*******_ cada
******** dígito tiene el mismo valor\n que el sistema decimal

***_ un 
**** byte 
*****_ es una
****** palabra de 8 bits

***_ su 
**** Facil conversión
*****_ a 
****** Sistema binario
****** Sistema Decimal

@endmindmap
```

[Sistema octal](https://es.wikipedia.org/wiki/Sistema_octal)